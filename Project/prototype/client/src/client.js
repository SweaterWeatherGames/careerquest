
const sock = io();
/*
0 = home page
1 = waiting host
2 = active host
-1 = guest (not a host)
*/
let guestScreenKey = 'home';
let prevScreenKey = 'home';
let assignPrevKey = 'home';

let step = 0;

let player = null;

const switchToHome = () => {

	var onLoad = function(){
		var button = document.getElementById('host-button');
		button.addEventListener('click', () => {
			sock.emit('req-host', '');
		});
		button = document.getElementById('join-button');
		button.addEventListener('click', () => {
			switchToGuestJoin();
		});

		player = null;
	}
	setHTML(home, onLoad);

};

const queryRoomCode = (code) => {
	sock.emit('client-rcode', code);
};

const switchToHostWait = (roomcode) => {
	var onLoad = function(){

		document.getElementById('room-code').innerHTML = roomcode;
		var button = document.getElementById('start-button');
		button.addEventListener('click', () => {
			//sock.emit('turn', id);
			switchToHost(roomcode);
			sock.emit('start-game', '');
		});
	}
	setHTML(hostWait, onLoad);
};

const switchToHost = (roomcode) => {
	var onLoad = function(){
		document.getElementById('room-code').innerHTML = roomcode;
	}
	setHTML(host, onLoad);
};

const switchToGuestJoin = () => {

	var onLoad = function(){


		var button = document.getElementById('join-button');
		button.addEventListener('click', () => {
			if(document.getElementById('room-code').value != ''){
				sock.emit('req-join', [document.getElementById('room-code').value, document.getElementById('name-field').value]);
			}
		});
	}

	setHTML(guestJoin, onLoad);
};

const switchToGuestWait = (roomcode) => {
	var temp = document.getElementById('name-field').value;

	var onLoad = function(){

		document.getElementById('room-code').innerHTML = roomcode;
		//document.getElementById('name-field').value = temp;
	}

	setHTML(guestWait, onLoad);
};

const switchToPickComp = (playerID) => {

	var onLoad = function(){
		//var button = document.getElementById('submit');
		var radios = document.getElementsByName('company');
		for (var n = 0; n < radios.length; n++){
			radios[n].addEventListener('click', ()=>{
				var companyPick = '';

				for (var i = 0; i < radios.length; i++)
				{
				 if (radios[i].checked)
				 {
				  // do whatever you want with the checked radio
				  companyPick = radios[i].value;

				  // only one radio can be logically checked, don't check the rest
				  break;
				 }
				}
				switchToConfirmComp(companyPick);
			});
		}
	}

	setHTML(chooseComp, onLoad);


};

const switchToConfirmComp = (pick) =>{
	var onLoad = function(){
		var parent = doc('confirmCompanyPage');

		if(pick != 'manu')parent.removeChild(doc('manu'));
		if(pick != 'i.t.')parent.removeChild(doc('i.t.'));
		if(pick != 'heal')parent.removeChild(doc('heal'));
		if(pick != 'cons')parent.removeChild(doc('cons'));

		doc('submit').addEventListener('click', ()=>{
			emitTurn('PickComp', pick);
		});
		doc('cancel').addEventListener('click', ()=>{
			switchToPickComp(0);
		});
	}

	setHTML(confirmComp, onLoad)
};

const switchToCompWait = () =>{
	var onLoad = function(){
		var parent = document.getElementById('waitCompanyPage');
		var heal = document.getElementById('heal');
		var i_t_ = document.getElementById('i.t.');
		var cons = document.getElementById('cons');
		var manu = document.getElementById('manu');

		if(player.company.type != 'heal')parent.removeChild(heal);
		if(player.company.type != 'i.t.')parent.removeChild(i_t_);
		if(player.company.type != 'cons')parent.removeChild(cons);
		if(player.company.type != 'manu')parent.removeChild(manu);
	}
	setHTML(waitCompany, onLoad);
}

const setHeader = (active) =>{
	document.getElementById("worker-button").disabled = !active;
	document.getElementById("events-button").disabled = !active;
	document.getElementById("goal-button").disabled = !active;

	if(!active){
		doc('worker-button').classList.add('full-hidden');
		doc('events-button').classList.add('full-hidden');
		doc('goal-button').classList.add('full-hidden');
	}

	document.getElementById("money").innerHTML = '$' + player.company.money;
	document.getElementById("goals").innerHTML = player.company.v_pnt;

	if(player.company.money > 0)doc('money').classList.add('green');
	else if(player.company.money < 0)doc('money').classList.add('red');
	else doc('money').classList.add('grey');

	if(player.event.name == null)doc('events-button').disabled = true;

	doc('worker-button').addEventListener('click', ()=>{
		//switchToCheckWorkers();
		switchToWorkerAssing(guestScreenKey);
	});
	doc('events-button').addEventListener('click', ()=>{
		switchToCheckEvent();
	});
	doc('goal-button').addEventListener('click', ()=>{
		switchToGoal();
	});
}
const setFooter = () =>{
	//TODO: add footer setup logic here later.
	doc('footer').innerHTML = '';
}

const switchToFirstDraw = () =>{
	var onLoad = function(){

		setHeader(true);
		setFooter();
		document.getElementById("worker-1").addEventListener('click', () =>{
			switchToWorkerDetails(0, guestScreenKey);
		});
		//document.getElementById("worker-1").innerHTML = player.draw.workers[0].type;
		if(player.selectedWorkers != undefined && player.selectedWorkers.indexOf(player.draw.workers[0]) != -1) doc("worker-1").disabled = true;
		doc('img1').src = player.draw.workers[0].imgURL;

		document.getElementById("worker-2").addEventListener('click', () =>{
			switchToWorkerDetails(1, guestScreenKey);
		});
		//document.getElementById("worker-2").innerHTML = player.draw.workers[1].type;
		if(player.selectedWorkers != undefined && player.selectedWorkers.indexOf(player.draw.workers[1]) != -1) doc("worker-2").disabled = true;
		doc('img2').src = player.draw.workers[1].imgURL;

		document.getElementById("submit").addEventListener('click', () =>{
			switchToProjectDraw();
		});

		doc('worker-button').disabled = true;
		doc('events-button').disabled = true;
	}

	setHTML(firstDraw, onLoad);
}

const switchToDrawWorkers = () =>{
	var onLoad = function(){

		setHeader(true);
		setFooter();
		if(player.selectedWorkers == undefined)player.selectedWorkers = [];

		document.getElementById("worker-1").addEventListener('click', () =>{
			switchToWorkerDetails(0, guestScreenKey);
		});
		//document.getElementById("worker-1").innerHTML = player.draw.workers[0].type;
		if(player.selectedWorkers != undefined && player.selectedWorkers.indexOf(player.draw.workers[0]) != -1) doc("worker-1").disabled = true;
		doc('img1').src = player.draw.workers[0].imgURL;

		document.getElementById("worker-2").addEventListener('click', () =>{
			switchToWorkerDetails(1, guestScreenKey);
		});
		//document.getElementById("worker-2").innerHTML = player.draw.workers[1].type;
		if(player.selectedWorkers != undefined && player.selectedWorkers.indexOf(player.draw.workers[1]) != -1) doc("worker-2").disabled = true;
		doc('img2').src = player.draw.workers[1].imgURL;

		document.getElementById("worker-3").addEventListener('click', () =>{
			switchToWorkerDetails(2, guestScreenKey);
		});
		//document.getElementById("worker-3").innerHTML = player.draw.workers[2].type;
		if(player.selectedWorkers != undefined && player.selectedWorkers.indexOf(player.draw.workers[2]) != -1) doc("worker-3").disabled = true;
		doc('img3').src = player.draw.workers[2].imgURL;

		document.getElementById("worker-4").addEventListener('click', () =>{
			switchToWorkerDetails(3, guestScreenKey);
		});
		//document.getElementById("worker-4").innerHTML = player.draw.workers[2].type;
		if(player.selectedWorkers != undefined && player.selectedWorkers.indexOf(player.draw.workers[3]) != -1) doc("worker-4").disabled = true;
		doc('img4').src = player.draw.workers[3].imgURL;


		if(player.company.workers.length + player.selectedWorkers.length >= 5){
			doc("worker-1").disabled = true;
			doc("worker-2").disabled = true;
			doc("worker-3").disabled = true;
			doc("worker-4").disabled = true;
			doc('update').innerHTML = "Corporate says, you've reached the maximum amount of assets we can afford!  If you wish to hire anyone new, you must let go of 1 or more employees.";
		}


		var parent = doc('manu');
		if(player.event.name != "It’s a Variety!"){
			parent.removeChild(doc("worker-4"));
			doc("worker-3").classList.add('center');
		}
		if(player.event.name == "Limited Supply" || player.event.name == "Slow Season")parent.removeChild(doc("worker-3"));
		if(player.event.name == "Slow Season"){
			parent.removeChild(doc("worker-2"));
			doc("worker-1").classList.add('center');
		}

		document.getElementById("submit").addEventListener('click', () =>{
			switchToProjectDraw();
		});
	}

	setHTML(drawWorkers, onLoad);
}

const switchToProjectDraw = () =>{
	var onLoad = function(){
		step = 0;

		setHeader(true);
		setFooter();
		if(player.selectedProjects == undefined)player.selectedProjects = [];
		
		doc('project-1').addEventListener('click', () =>{
			switchToProjectDetails(0, guestScreenKey);
		});
		//doc('project-1').innerHTML = player.draw.projects[0].type;
		if(player.selectedProjects != undefined && player.selectedProjects.indexOf(player.draw.projects[0]) != -1) doc("project-1").disabled = true;
		doc('img1').src = player.draw.projects[0].imgURL;
		
		doc('project-2').addEventListener('click', () =>{
			switchToProjectDetails(1, guestScreenKey);
		});
		//doc('project-2').innerHTML = player.draw.projects[1].type;
		if(player.selectedProjects != undefined && player.selectedProjects.indexOf(player.draw.projects[1]) != -1) doc("project-2").disabled = true;
		doc('img2').src = player.draw.projects[1].imgURL;
		
		doc('project-3').addEventListener('click', () =>{
			switchToProjectDetails(2, guestScreenKey);
		});
		//doc('project-3').innerHTML = player.draw.projects[2].type;
		if(player.selectedProjects != undefined && player.selectedProjects.indexOf(player.draw.projects[2]) != -1) doc("project-3").disabled = true;
		doc('img3').src = player.draw.projects[2].imgURL;
		
		doc('project-4').addEventListener('click', () =>{
			switchToProjectDetails(3, guestScreenKey);
		});
		//doc('project-4').innerHTML = player.draw.projects[3].type;
		if(player.selectedProjects != undefined && player.selectedProjects.indexOf(player.draw.projects[3]) != -1) doc("project-4").disabled = true;
		doc('img4').src = player.draw.projects[3].imgURL;


		if(player.company.projects.length + player.selectedProjects.length >= 5){
			doc("project-1").disabled = true;
			doc("project-2").disabled = true;
			doc("project-3").disabled = true;
			doc("project-4").disabled = true;
			doc('update').innerHTML = "Corporate says, you've reached the maximum capacity of of projects we can support!  If you wish to take on anymore, you must complete some first.";
		}


		var parent = doc('manu');
		if(player.event.name != "It’s a Variety!"){
			parent.removeChild(doc("project-4"));
			doc("project-3").classList.add('center');
		}
		if(player.event.name == "Limited Supply" || player.event.name == "Slow Season")parent.removeChild(doc("project-3"));
		if(player.event.name == "Slow Season"){
			parent.removeChild(doc("project-2"));
			doc("project-1").classList.add('center');
		}

		document.getElementById("submit").addEventListener('click', () =>{
			//switchToWorkerAssing();
			var arr = [
						(player.selectedWorkers)?(player.selectedWorkers.indexOf(player.draw.workers[0]) != -1) : false,
						(player.selectedWorkers)?(player.selectedWorkers.indexOf(player.draw.workers[1]) != -1) : false,
						(player.selectedWorkers)?(player.selectedWorkers.indexOf(player.draw.workers[2]) != -1) : false,
						(player.selectedWorkers)?(player.selectedWorkers.indexOf(player.draw.workers[3]) != -1) : false,
						(player.selectedProjects)?(player.selectedProjects.indexOf(player.draw.projects[0]) != -1) : false,
						(player.selectedProjects)?(player.selectedProjects.indexOf(player.draw.projects[1]) != -1) : false,
						(player.selectedProjects)?(player.selectedProjects.indexOf(player.draw.projects[2]) != -1) : false,
						(player.selectedProjects)?(player.selectedProjects.indexOf(player.draw.projects[3]) != -1) : false
						];
			emitTurn('AccDraw', arr);
		});
		doc('cancel').addEventListener('click', ()=>{
			if(player.event.name != null)switchToDrawWorkers();
			else switchToFirstDraw();
		});

	}

	setHTML(drawProjects, onLoad);
}

const switchToWorkerDetails = (index, prevScreen) =>{
	var onLoad = function(){
		setHeader(false);
		setFooter();
		if(player.selectedWorkers == undefined)player.selectedWorkers = [];

		doc('hire-cost').innerHTML = ' $' + player.draw.workers[index].hire;
		switch(player.draw.workers[index].type){
			case 'manu':
				doc('type').innerHTML = 'Manufacturing';
				break;
			case 'cons':
				doc('type').innerHTML = 'Construction';
				break;
			case 'heal':
				doc('type').innerHTML = 'Health';
				break;
			case 'i.t.':
				doc('type').innerHTML = 'IT';
				break;
		}
		
		doc('salary').innerHTML = ' $' + player.draw.workers[index].salary;
		doc('skill').innerHTML = player.draw.workers[index].skill;
		doc('img').src = player.draw.workers[index].imgURL;
		if(!player.draw.workers[index].leader)doc('manu').removeChild(doc('leader'));

		document.getElementById('submit').addEventListener('click', () =>{
			if(player.company.workers.length + player.selectedWorkers.length >= 5)return;
			player.selectedWorkers.push(player.draw.workers[index]);
			player.company.money -= player.draw.workers[index].hire;
			switch(prevScreen){
				case firstDraw:
					switchToFirstDraw();
					break;
				case drawWorkers:
					switchToDrawWorkers();
					break;
				case drawProjects:
					switchToProjectDraw();
					break;
				case workerAssign:
					switchToWorkerAssing(guestScreenKey);
			}
		});
		document.getElementById('cancel').addEventListener('click', () =>{
			switch(prevScreen){
				case firstDraw:
					switchToFirstDraw();
					break;
				case drawWorkers:
					switchToDrawWorkers();
					break;
				case drawProjects:
					switchToProjectDraw();
					break;
				case workerAssign:
					switchToWorkerAssing(guestScreenKey);
			}
		});
	}

	setHTML(workerDetails, onLoad);
}

const switchToProjectDetails = (index, prevScreen) =>{
	var onLoad = function(){
		setHeader(false);
		setFooter();
		if(player.selectedProjects == undefined)player.selectedProjects = [];

		doc('img').src = player.draw.projects[index].imgURL;
		doc('payoff').innerHTML = player.draw.projects[index].value;
		doc('duration').innerHTML = player.draw.projects[index].duration;
		doc('tier').innerHTML = player.draw.projects[index].tier;
		switch(player.draw.projects[index].type){
			case 'manu':
				doc('type').innerHTML = 'Manufacturing';
				break;
			case 'cons':
				doc('type').innerHTML = 'Construction';
				break;
			case 'heal':
				doc('type').innerHTML = 'Health';
				break;
			case 'i.t.':
				doc('type').innerHTML = 'IT';
				break;
		}

		document.getElementById('submit').addEventListener('click', () =>{
			if(player.company.projects.length + player.selectedProjects.length >= 5)return;
			player.selectedProjects.push(player.draw.projects[index]);
			switch(prevScreen){
				case firstDraw:
					switchToFirstDraw();
					break;
				case drawWorkers:
					switchToDrawWorkers();
					break;
				case drawProjects:
					switchToProjectDraw();
					break;
				case workerAssign:
					switchToWorkerAssing(guestScreenKey);
			}
		});
		document.getElementById('cancel').addEventListener('click', () =>{
			switch(prevScreen){
				case firstDraw:
					switchToFirstDraw();
					break;
				case drawWorkers:
					switchToDrawWorkers();
					break;
				case drawProjects:
					switchToProjectDraw();
					break;
				case workerAssign:
					switchToWorkerAssing(guestScreenKey);
			}
		});
	}

	setHTML(projectDetails, onLoad);
}

const switchToWorkerAssing = (prev) =>{
	var onLoad = function(){
		assignPrevKey = prev;
		setHeader(false);
		setFooter();

		var workers = doc('workers');
		var projects = doc('Projects');
		var workers2 = doc('workers2');
		var projects2 = doc('Projects2');

		var workerHTML = workers.innerHTML;
		var projectHTML = projects.innerHTML;
		var workerHTML2 = workers2.innerHTML;
		var projectHTML2 = projects2.innerHTML;

		var assignedProjects = [];

		if(player.company.workers.length <= 3){
			player.company.workers.forEach((worker, index)=>{
				workerHTML+='<span id="wspan' + index + '"><img class="workerIcon" ' + 'id="work' + index + '"><span>' + worker.skill + ' skill</span></span>';

				workers.innerHTML = workerHTML;
				
			});
			doc('checkWorkerList').removeChild(doc('workers2'));
		}
		else{
			player.company.workers.forEach((worker, index)=>{
				if(index%2 != 0)workerHTML+='<span id="wspan' + index + '"><img class="workerIcon" ' + 'id="work' + index + '"><span>' + worker.skill + ' skill</span></span>';
				else workerHTML2+='<span id="wspan' + index + '"><img class="workerIcon" ' + 'id="work' + index + '"><span>' + worker.skill + ' skill</span></span>';

				workers.innerHTML = workerHTML;
				workers2.innerHTML = workerHTML2;

			});
			//doc('checkWorkerList').classList.add('skinny');
		}
		player.company.workers.forEach((worker, index)=>{

			doc('work'+index).addEventListener('click', () =>{
				//var temp = prevScreenKey;

				switchToWorkerStatus(prev, index);

				//prevScreenKey = temp;
			});
			doc('work'+index).src = worker.imgURL;
			var newClass = '';
			if(worker.project != null){
				var projIndex = 0;
				player.company.projects.forEach((p, index2)=>{
					if(worker.project.index === p.index)projIndex = index2;
				});
				switch(projIndex){
					case 0:
						newClass = 'red-outline';
						break;
					case 1:
						newClass = 'green-outline';
						break;
					case 2:
						newClass = 'blue-outline';
						break;
					case 3:
						newClass = 'purple-outline';
						break;
					case 4:
						newClass = 'teal-outline';
						break;
				}
				assignedProjects.push(worker.project.index);
			}
			if(newClass != '')doc('work'+index).classList.add(newClass);
		});

		if(player.company.projects.length <= 3){
			player.company.projects.forEach((project, index)=>{
				projectHTML+='<span id="pspan' + index + '"><img class="projectIcon" ' + 'id="proj' + index + '"><span>' + project.duration + ' turns</span></span>';

				projects.innerHTML = projectHTML;
				

			});
			doc('checkWorkerList').removeChild(doc('Projects2'));
		}
		else{
			player.company.projects.forEach((project, index)=>{
				if(index%2 != 0)projectHTML+='<span id="pspan' + index + '"><img class="projectIcon" ' + 'id="proj' + index + '"><span>' + project.duration + ' turns</span></span>';
				else projectHTML2+='<span id="pspan' + index + '"><img class="projectIcon" ' + 'id="proj' + index + '"><span>' + project.duration + ' turns</span></span>';

				projects.innerHTML = projectHTML;
				projects2.innerHTML = projectHTML2;

			});
			doc('checkWorkerList').classList.add('skinny');
		}
		player.company.projects.forEach((project, index)=>{
			doc('proj'+index).addEventListener('click', () =>{
				//var temp = prevScreenKey;

				switchToProjectStatus(index, prev);
				//prevScreenKey = temp;
			});
			doc('proj'+index).src = project.imgURL;
			if(step == 0){//flash
				doc('proj'+index).classList.add('flash-button');
			}
			var newClass = '';
			if(assignedProjects.indexOf(project.index) != -1){

				switch(index){
					case 0:
						newClass = 'red-outline';
						break;
					case 1:
						newClass = 'green-outline';
						break;
					case 2:
						newClass = 'blue-outline';
						break;
					case 3:
						newClass = 'purple-outline';
						break;
					case 4:
						newClass = 'teal-outline';
						break;
				}
			}
			if(newClass != '')doc('proj'+index).classList.add(newClass);
		});


		if(player.company.projects.length >= 4 && player.company.workers.length >= 4 ||
			player.company.projects.length == 1 && player.company.workers.length >= 4 ||
			player.company.projects.length >= 4 && player.company.workers.length == 1 ||
			player.company.projects.length == 1 && player.company.workers.length == 1){
			doc('checkWorkerList').classList.add('smallest');
		}
		else if(player.company.projects.length >= 4 || player.company.projects.length == 1 ||
			  player.company.workers.length >= 4 || player.company.workers.length == 1){
			doc('checkWorkerList').classList.add('skinny');
		}

		doc('submit').addEventListener('click', ()=>{
			console.log(prev);

			if(assignPrevKey == end)switchToEnd();
			else if(assignPrevKey == drawWorkers)switchToDrawWorkers();
			else if(assignPrevKey == firstDraw)switchToFirstDraw();
			else if(assignPrevKey == drawProjects)switchToProjectDraw();

		});
		if(step == 1){//flash
			doc('submit').classList.add('flash-button');
			doc('tip').innerHTML = `When you're done managing your assets, click the back arrow to return to the main screen.`
		}
		step = 1;
	}

	setHTML(workerAssign, onLoad);
}

const switchToWorkerStatus = (prev, index) =>{
	var onLoad = function(){
		setHeader(false);
		setFooter();
		if(prev == forcedFire){
			document.getElementsByClassName('input-wrapper')[0].removeChild(doc('train-button'));
			document.getElementsByClassName('input-wrapper')[0].removeChild(doc('cancel'));
			doc('worker').removeChild(doc('project-button'));
			document.getElementsByClassName('input-wrapper')[0].removeChild(doc('modify-button'));
		}else if (prev == modifyWorker){
			document.getElementsByClassName('input-wrapper')[0].removeChild(doc('train-button'));
			document.getElementsByClassName('input-wrapper')[0].removeChild(doc('cancel'));
			doc('worker').removeChild(doc('project-button'));
			document.getElementsByClassName('input-wrapper')[0].removeChild(doc('fire-button'));
			doc('modify-button').addEventListener('click', ()=>{
				emitTurn('modify', index);
			});
		}
		else {
			document.getElementsByClassName('input-wrapper')[0].removeChild(doc('modify-button'));
			doc('train-button').addEventListener('click', ()=>{
				doc('worker').removeChild(doc('project-button'));
				emitTurn('Assign', [-1, index]);
			});
			doc('cancel').addEventListener('click', ()=>{
				switchToWorkerAssing(prev);
			});
			if(player.company.workers[index].project != null){
				doc('project-button').addEventListener('click', ()=>{
					var projIndex = 0;
					player.company.projects.forEach((p, pindex)=>{
						if(p.index === player.company.workers[index].project.index) projIndex = pindex;
					});
					switchToProjectStatus(projIndex, prev);
				});
			}
			else doc('worker').removeChild(doc('project-button'));
		}

		switch(player.company.workers[index].type){
			case 'i.t.':
				doc('type').innerHTML = 'IT Worker';
				break;
			case 'manu':
				doc('type').innerHTML = 'Manufacturing Worker';
				break;
			case 'cons':
				doc('type').innerHTML = 'Construction Worker';
				break;
			case 'heal':
				doc('type').innerHTML = 'Health Worker';
				break;
		}
		//doc('type').innerHTML = player.company.workers[index].type;
		doc('img').src = player.company.workers[index].imgURL;
		doc('salary').innerHTML = player.company.workers[index].salary;
		doc('skill').innerHTML = player.company.workers[index].skill;
		if(!player.company.workers[index].leader)doc('worker').removeChild(doc('leader'));
		if(!player.company.workers[index].sick)doc('worker').removeChild(doc('sick'));
		if(doc('fire-button'))doc('fire-button').addEventListener('click', ()=>{
			emitTurn('FireWork', index);
		});

	}

	setHTML(workerStatus, onLoad);
}

const switchToProjectStatus = (num, prev) =>{
	var onLoad = function(){
		setHeader(false);
		setFooter();

		var workersHere = doc('workersHere');
		var workersOther = doc('workersOther');
		var workersTrain = doc('workersTrain');
		var workerHereHTML = workersHere.innerHTML;
		var workerOtherHTML = workersOther.innerHTML;
		var workerTrainHTML = workersTrain.innerHTML;

		var project = player.company.projects[num];

				//workerHTML+='<span id="wspan' + index + '"><img class="workerIcon" ' + 'id="work' + index + '"><span>' + worker.skill + ' skill</span></span>';

		player.company.workers.forEach((worker, index)=>{
			if(project.type === worker.type){
				if(worker.project && worker.project.index === project.index)workerHereHTML+='<span id="wspan' + index + '"><img class="workerIcon" ' + 'id="work' + index + '"><span>' + worker.skill + ' skill</span></span>';
				else if(worker.project != null)workerOtherHTML+='<span id="wspan' + index + '"><img class="workerIcon flash-button" ' + 'id="work' + index + '"><span>' + worker.skill + ' skill</span></span>';
				else workerTrainHTML+='<span id="wspan' + index + '"><img class="workerIcon flash-button" ' + 'id="work' + index + '"><span>' + worker.skill + ' skill</span></span>';
			}
			
			workersHere.innerHTML = workerHereHTML;
			workersOther.innerHTML = workerOtherHTML;
			workersTrain.innerHTML = workerTrainHTML;

			if(workersHere.innerHTML === '<h1>Workers assigned to this project.</h1>')workersHere.innerHTML += 'NONE...';
			if(workersOther.innerHTML === '<h1>Workers assigned to other projects.  Tap one to change their project to this project.</h1>')workersOther.innerHTML += 'NONE...';
			if(workersTrain.innerHTML === '<h1>Compatible workers in training.  Tap one to end their training and put them on this project.</h1>')workersTrain.innerHTML += 'NONE...';
		});
		player.company.workers.forEach((worker, index)=>{
			if(doc('work'+index))doc('work'+index).addEventListener('click', () =>{
				emitTurn('Assign', [num, index]);
			});
			if(doc('work'+index))doc('work'+index).src = worker.imgURL;
		});

		doc('cancel').addEventListener('click', ()=>{
			switchToWorkerAssing(prev);
		});

		doc('img').src = project.imgURL;
		doc('payoff').innerHTML = project.value;
		doc('duration').innerHTML = project.duration;
		doc('tier').innerHTML = project.tier;
		//doc('type').innerHTML = project.type;
		switch(project.type){
			case 'i.t.':
				doc('type').innerHTML = 'IT Project';
				break;
			case 'manu':
				doc('type').innerHTML = 'Manufacturing Project';
				break;
			case 'cons':
				doc('type').innerHTML = 'Construction Project';
				break;
			case 'heal':
				doc('type').innerHTML = 'Health Project';
				break;
		}
	}

	setHTML(projectStatus, onLoad);
}

const switchToForceFire =(shouldLayoff) =>{
	var onLoad = function(){
		setHeader(false);
		setFooter();

		var workers = doc('workers');

		var workerHTML = workers.innerHTML;

		if(shouldLayoff != null)doc('reason').innerHTML = 'You must fire an employee due to downsizing.';


		var workers = doc('workers');
		var workers2 = doc('workers2');

		var workerHTML = workers.innerHTML;
		var workerHTML2 = workers2.innerHTML;

		if(player.company.workers.length <= 3){
			player.company.workers.forEach((worker, index)=>{
				workerHTML+='<span id="wspan' + index + '"><img class="workerIcon" ' + 'id="work' + index + '"><span>' + worker.skill + ' skill</span></span>';

				workers.innerHTML = workerHTML;
				
			});
			doc('checkWorkerList').removeChild(doc('workers2'));
		}
		else{
			player.company.workers.forEach((worker, index)=>{
				if(index%2 != 0)workerHTML+='<span id="wspan' + index + '"><img class="workerIcon" ' + 'id="work' + index + '"><span>' + worker.skill + ' skill</span></span>';
				else workerHTML2+='<span id="wspan' + index + '"><img class="workerIcon" ' + 'id="work' + index + '"><span>' + worker.skill + ' skill</span></span>';

				workers.innerHTML = workerHTML;
				workers2.innerHTML = workerHTML2;

			});
			//doc('checkWorkerList').classList.add('skinny');
		}
		player.company.workers.forEach((worker, index)=>{

			doc('work'+index).addEventListener('click', () =>{

				switchToWorkerStatus(guestScreenKey, index);
			});
			doc('work'+index).src = worker.imgURL;
		});
	}

	setHTML(forcedFire, onLoad);
}

const switchToModifyWorker = () =>{
	var onLoad = function(){
		setHeader(false);
		setFooter();
		var workers = doc('workers');

		var workerHTML = workers.innerHTML;

		var text = '';

		switch(player.event.name){
			case "Fast Learner":
				text = "Select a worker to recieve +1 efficiency!"
				break;
			case "Calling in Sick":
				text = "Select a worker who will call in sick.  They will not be able to work on projects this turn..."
				break;
			case "Fast-er Learner":
				text = "Select a worker to recieve +2 efficiency!"
				break;
			case "Lack of Productivity":
				text = "Select a worker to lose -2 efficiency..."
				break;
		}

		doc('reason').innerHTML = text;

		/*player.company.workers.forEach((worker, index)=>{
			workerHTML+='<button class="workerIcon" ' + 'id="work' + index + '">' + worker.type + " " + worker.skill + '</button>';

			workers.innerHTML = workerHTML;

		});
		player.company.workers.forEach((worker, index)=>{


			doc('work'+index).addEventListener('click', () =>{
				switchToWorkerStatus(guestScreenKey, index);
			});
		});*/

		var workers = doc('workers');
		var workers2 = doc('workers2');

		var workerHTML = workers.innerHTML;
		var workerHTML2 = workers2.innerHTML;

		if(player.company.workers.length <= 3){
			player.company.workers.forEach((worker, index)=>{
				workerHTML+='<span id="wspan' + index + '"><img class="workerIcon" ' + 'id="work' + index + '"><span>' + worker.skill + ' skill</span></span>';

				workers.innerHTML = workerHTML;
				
			});
			doc('checkWorkerList').removeChild(doc('workers2'));
		}
		else{
			player.company.workers.forEach((worker, index)=>{
				if(index%2 != 0)workerHTML+='<span id="wspan' + index + '"><img class="workerIcon" ' + 'id="work' + index + '"><span>' + worker.skill + ' skill</span></span>';
				else workerHTML2+='<span id="wspan' + index + '"><img class="workerIcon" ' + 'id="work' + index + '"><span>' + worker.skill + ' skill</span></span>';

				workers.innerHTML = workerHTML;
				workers2.innerHTML = workerHTML2;

			});
			//doc('checkWorkerList').classList.add('skinny');
		}
		player.company.workers.forEach((worker, index)=>{

			doc('work'+index).addEventListener('click', () =>{

				switchToWorkerStatus(guestScreenKey, index);
			});
			doc('work'+index).src = worker.imgURL;
		});
	}

	setHTML(modifyWorker, onLoad);
}

const switchToEnd = () =>{
	var onLoad = function(){
		setHeader(true);
		setFooter();
		/*doc('trade-button').addEventListener('click', ()=>{
			switchToWorkerAssing();
		});*/
		doc('pay-button').addEventListener('click', ()=>{
			switchToPayWorkers();
		});
		doc('end-button').addEventListener('click', ()=>{
			emitTurn('endTurn');
		});
		doc('name').innerHTML = player.name;
		//doc('worker-button').removeEventListener('click', null);
		//doc('worker-button').addEventListener('click', ()=>{
		//	switchToWorkerAssing();
		//});
		if(!player.company.workersPaid)doc('end-button').disabled = true;
		else doc('pay-button').disabled = true;

		switch(step){
			case 0:
				doc('worker-button').classList.add('flash-button');
				break;
			case 1:
				doc('goal-button').classList.add('flash-button');
				doc('tip').innerHTML = `Now might be a good time to check in on your goals tab to see how close you are to winning.  Complete all 4 goals first to win!`;
				step = 2;
				break;
			case 2:
				doc('pay-button').classList.add('flash-button');
				doc('tip').innerHTML = `You can't finish your turn before you pay your workers!`;
				break;
			case 3:
				doc('end-button').classList.add('flash-button');
				doc('tip').innerHTML = `Looks like you're ready to end your turn.  If you forgot to do anything you think you still need to, it isn't too late to go back and do them!`;
				break;
		}
	}
	setHTML(end, onLoad);
}

const switchToPayWorkers = () =>{
	var onLoad = function(){
		setHeader(false);
		setFooter();
		//emitTurn('PayWork', '');
		var cost = 0;
		player.company.workers.forEach((w)=>{
			cost+=w.salary;
		});
		doc('cost').innerHTML = cost;
		doc('remaining').innerHTML = parseFloat(parseInt((player.company.money - cost) * 100))/100;
		doc('cancel').addEventListener('click', ()=>{
			switchToEnd();
		});
		doc('submit').addEventListener('click', ()=>{
			emitTurn('PayWork', '');
		});
		step = 3;
	}

	setHTML(payConfirm, onLoad);
}

const switchToWait = () =>{
	var onLoad = function(){

	}

	setHTML(wait, onLoad);
}

const switchToProjectComplete = () =>{
	var onLoad = function(){
		step = 0;

		setHeader(false);
		setFooter();

		var doneProj = doc('doneProjects');
		var wipProj = doc('wipProjects');

		var doneProjHTML = doneProj.innerHTML;
		var wipProjHTML = wipProj.innerHTML;

		
	
		player.company.doneProjects.forEach((project, index)=>{
			doneProjHTML+='<span id="pspan' + index + '"><img class="projectIcon" ' + 'id="doneProj' + index + '"><span>+ $' + project.value + '!</span></span>';

			doneProj.innerHTML = doneProjHTML;
			

		});
		player.company.projects.forEach((project, index)=>{
			wipProjHTML+='<span id="pspan' + index + '"><img class="projectIcon" ' + 'id="wipProj' + index + '"><span>' + project.duration + ' turns left</span></span>';

			wipProj.innerHTML = wipProjHTML;
			

		});

		let int = 0;
		player.company.doneProjects.forEach((project, index)=>{
			doc('doneProj'+index).src = project.imgURL;
			int += project.value;
		});
		player.company.projects.forEach((project, index)=>{
			doc('wipProj'+index).src = project.imgURL;
		});
		
		if(player.company.doneProjects.length == 0){
			doc('update').innerHTML = "You're company didn't complete any assignments this turn...";
			doc('checkWorkerList').removeChild(doc('doneProjects'));
			doc('checkWorkerList').removeChild(doc('header1'));
		}
		else doc('update').innerHTML = "You're company completed some projects, netting you a nice $" + int + "!";
		if(player.company.projects.length == 0){
			doc('checkWorkerList').removeChild(doc('wipProjects'));
			doc('checkWorkerList').removeChild(doc('header2'));
		}


		if(player.company.projects.length == 1 || player.company.doneProjects.length == 1){
			doc('checkWorkerList').classList.add('smallest');
		}
		
		doc('submit').addEventListener('click',()=>{
			switchToDrawWorkers();
		}); 
	}

	setHTML(projectCompete, onLoad);
}

const switchToGoal = () =>{
	var onLoad = function(){
		setHeader(false);
		setFooter();
		//money
		if(player.company.money >= 25)doc('goal-1').innerHTML = "<span class='green'>☑</span> You have $" + player.company.money + " out of your goal of $25!";
		else doc('goal-1').innerHTML = "<span class='red'>☐</span> You only have $" + player.company.money + " out of your goal of $25... Get $" + (25 - player.company.money) + " more.";
		//diversity
		if(player.diversity == 4)doc('goal-2').innerHTML = "<span class='green'>☑</span> You have one of every worker type!";
		else doc('goal-2').innerHTML = "<span class='red'>☐</span> You only have " + player.diversity + " of the worker types... get one of every type.";
		//level 3 project
		if(player.level3s >= 3)doc('goal-3').innerHTML = "<span class='green'>☑</span> You have completed " + player.level3s + " of your goal of 3 tier 3 projects!";
		else doc('goal-3').innerHTML = "<span class='red'>☐</span> You only have completed " + player.level3s + " out of your goal of 3 level 3 projects... Complete " + (3 - player.level3s) + " more.";
		//leadership
		if(player.hasLeader)doc('goal-4').innerHTML = "<span class='green'>☑</span> You have trained a project leader!";
		else doc('goal-4').innerHTML = "<span class='red'>☐</span> You don't have any project leaders... Either recruit one or train a current worker to level 6.";

		switch(player.company.type){
			case 'cons':
				doc('goalPage').removeChild(doc('manuAbility'));
				doc('goalPage').removeChild(doc('i.t.Ability'));
				doc('goalPage').removeChild(doc('healAbility'));
				break;
			case 'manu':
				doc('goalPage').removeChild(doc('consAbility'));
				doc('goalPage').removeChild(doc('i.t.Ability'));
				doc('goalPage').removeChild(doc('healAbility'));
				break;
			case 'i.t.':
				doc('goalPage').removeChild(doc('consAbility'));
				doc('goalPage').removeChild(doc('manuAbility'));
				doc('goalPage').removeChild(doc('healAbility'));
				break;
			case 'heal':
				doc('goalPage').removeChild(doc('manuAbility'));
				doc('goalPage').removeChild(doc('i.t.Ability'));
				doc('goalPage').removeChild(doc('consAbility'));
				break;
		}


		doc('cancel').addEventListener('click', ()=>{
			if(prevScreenKey == drawWorkers) switchToDrawWorkers();
			if(prevScreenKey == firstDraw) switchToFirstDraw();
			if(prevScreenKey == drawProjects) switchToProjectDraw();
			if(prevScreenKey == workerAssign) switchToWorkerAssing(guestScreenKey);
			if(prevScreenKey == end) switchToEnd();
		});
	}

	setHTML(goals, onLoad);
}

const switchToWin = () =>{
	var onLoad = function(){

	}

	setHTML(win, onLoad); 
}

const switchToLose = () =>{
	var onLoad = function(){

	}

	setHTML(lose, onLoad); 
}

const switchToEvent = () =>{
	var onLoad = function(){
		setHeader(false);
		setFooter();
		doc('name').innerHTML = player.event.name;
		doc('description').innerHTML = player.event.description;

		doc('submit').addEventListener('click', () =>{ 
			switch(player.event.name){
				case "It’s a Variety!":
				case "HR Demands Diversity":
				case "HR Demands Diversity":
				case "Limited Supply":
				case "Desperate Interns":
				case "Slow Season":
				case "Bonus Check!":
				case "Tax Returns":
				case "Taxes Are Due":
				case "Raise the Morale!":
					switchToProjectComplete();
					break;
				case "Downsizing":
					if(player.company.type == "heal"){
						switchToProjectComplete();
					}
					else switchToForceFire(true);
					break;
				default:
					switchToModifyWorker();
					break;
			}
			
		});
	}

	console.log(player.event.name);

	setHTML(event, onLoad);
}

const switchToCheckEvent = () =>{
	var onLoad = function(){

		setHeader(false);
		setFooter();
		doc('name').innerHTML = player.event.name;
		doc('description').innerHTML = player.event.description;

		doc('cancel').addEventListener('click', () =>{ 
			switch(prevScreenKey){
				case drawWorkers:
					switchToDrawWorkers();
					break;
				case firstDraw:
					switchToFirstDraw();
					break;
				case drawProjects: 
					switchToProjectDraw();
					break;
				case workerAssign: 
					switchToWorkerAssing(guestScreenKey);
					break;
				case end: 
					switchToEnd();
					break;
			}
			
		});

	}

	setHTML(checkEvent, onLoad);
}

const switchToCheckWorkers =() =>{
	var onLoad = function(){
		setHeader(false);
		setFooter();

		var workers = doc('workers');
		var projects = doc('Projects');
		var workers2 = doc('workers2');
		var projects2 = doc('Projects2');

		var workerHTML = workers.innerHTML;
		var projectHTML = projects.innerHTML;
		var workerHTML2 = workers2.innerHTML;
		var projectHTML2 = projects2.innerHTML;

		if(player.company.workers.length <= 3){
			player.company.workers.forEach((worker, index)=>{
				workerHTML+='<span id="wspan' + index + '"><img class="workerIcon" ' + 'id="work' + index + '"><span>' + worker.skill + ' skill</span></span>';

				workers.innerHTML = workerHTML;
				
			});
			doc('checkWorkerList').removeChild(doc('workers2'));
		}
		else{
			player.company.workers.forEach((worker, index)=>{
				if(index%2 != 0)workerHTML+='<span id="wspan' + index + '"><img class="workerIcon" ' + 'id="work' + index + '"><span>' + worker.skill + ' skill</span></span>';
				else workerHTML2+='<span id="wspan' + index + '"><img class="workerIcon" ' + 'id="work' + index + '"><span>' + worker.skill + ' skill</span></span>';

				workers.innerHTML = workerHTML;
				workers2.innerHTML = workerHTML2;

			});
		}
		player.company.workers.forEach((worker, index)=>{

			doc('work'+index).src = worker.imgURL;
		});

		if(player.company.projects.length <= 3){
			player.company.projects.forEach((project, index)=>{
				projectHTML+='<span id="pspan' + index + '"><img class="projectIcon" ' + 'id="proj' + index + '"><span>' + project.duration + ' turns</span></span>';

				projects.innerHTML = projectHTML;
				

			});
			doc('checkWorkerList').removeChild(doc('Projects2'));
		}
		else{
			player.company.projects.forEach((project, index)=>{
				if(index%2 != 0)projectHTML+='<span id="pspan' + index + '"><img class="projectIcon" ' + 'id="proj' + index + '"><span>' + project.duration + ' turns</span></span>';
				else projectHTML2+='<span id="pspan' + index + '"><img class="projectIcon" ' + 'id="proj' + index + '"><span>' + project.duration + ' turns</span></span>';

				projects.innerHTML = projectHTML;
				projects2.innerHTML = projectHTML2;

			});
		}
		player.company.projects.forEach((project, index)=>{
			doc('proj'+index).src = project.imgURL;
		});

		if(player.company.projects.length >= 4 && player.company.workers.length >= 4 ||
			player.company.projects.length == 1 && player.company.workers.length >= 4 ||
			player.company.projects.length >= 4 && player.company.workers.length == 1 ||
			player.company.projects.length == 1 && player.company.workers.length == 1){
			doc('checkWorkerList').classList.add('smallest');
		}
		else if(player.company.projects.length >= 4 || player.company.projects.length == 1 ||
			  player.company.workers.length >= 4 || player.company.workers.length == 1){
			doc('checkWorkerList').classList.add('skinny');
		}


		//if(player.company.workers.length%2 != 0)doc('wspan'+String(player.company.workers.length-1)).classList.add('center');
		//if(player.company.projects.length%2 != 0)doc('pspan'+String(player.company.projects.length-1)).classList.add('center');

		doc('cancel').addEventListener('click', () =>{ 
			switch(prevScreenKey){
				case drawWorkers:
					switchToDrawWorkers();
					break;
				case firstDraw:
					switchToFirstDraw();
					break;
				case drawProjects: 
					switchToProjectDraw();
					break;
				case workerAssign: 
					switchToWorkerAssing(guestScreenKey);
					break;
				case end: 
					switchToEnd();
					break;
			}
			
		});

	}

	setHTML(checkWorkers, onLoad);
}

const setHTML = (newHTML, onLoad) => {
	var xhr = typeof XMLHttpRequest != 'undefined' ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
	xhr.open('get', newHTML, true);
	xhr.onreadystatechange = function() {
	    if (xhr.readyState == 4 && xhr.status == 200) { 
	        document.getElementById("page-content").innerHTML = xhr.responseText;
	        onLoad();

	        prevScreenKey = guestScreenKey;
	        guestScreenKey = newHTML;
	    } 
	}

	switch(newHTML){
		case goals: //blue
			document.body.style.backgroundColor = "#dfd6f3";
			break;
		case workerAssign: //teal
		case workerStatus: //teal
		case projectStatus: //teal
			document.body.style.backgroundColor = "#d6f3ea";
			break;
		case checkEvent: //purple
			document.body.style.backgroundColor = "#d6e8f3";
			break;
		default:
			document.body.style.backgroundColor = "#f3e9d6";
			break;
	}

	xhr.send();
};

const doc = (element) =>{
	return document.getElementById(element);
}

sock.on('res-host', switchToHostWait);
sock.on('join-fail', () => {
	document.getElementById('romecode-entry').innerHTML = 'That Room Does Not Exist';
});
sock.on('already-playing', () => {
	document.getElementById('romecode-entry').innerHTML = 'Cannot Join Mid-Game';
});
sock.on('join-suc', switchToGuestWait);

sock.on('play-count', (playercount) => {
	document.getElementById('players-joined').innerHTML = 'Number of Players: ' + playercount.length;

	var plvs = ``;
	playercount.forEach((p)=>{
		plvs += `<span><img src="/../imgs/PlayerJoined.png"><span>` + p + `</span></span>`;
	});	

	document.getElementById('player-visuals').innerHTML = plvs;
});
sock.on('game-end', switchToHome);

sock.on('client-begin', (init) => {
	player = init;

	switchToPickComp();
});

sock.on('win', ()=>{
	switchToWin();
});

sock.on('lose', ()=>{
	switchToLose();
});

sock.on('update', (update) =>{
	player = update;

	switch(guestScreenKey){
		case confirmComp:
			if(player.allWaiting) switchToFirstDraw();
			else switchToCompWait();
			break;
		case waitCompany:
			switchToFirstDraw();
			break;
		case drawProjects:
			//switchToWorkerAssing();
			switchToEnd();
			break;
		case workerAssign:
			//switchToWorkerAssing();
			break;
		case wait:
			switchToEvent();
			break;
		case payConfirm:
			if(player.mustFire == true) switchToForceFire();
			else switchToEnd();
			break;
		case projectStatus:
			switchToWorkerAssing(assignPrevKey);
			break;
		case workerStatus:
			if(player.allWaiting)switchToEvent();
			else if(player.waiting)switchToWait();
			else if (player.event.name == "Downsizing" || player.event.name == "Fast Learner" || player.event.name == "Calling in Sick" || player.event.name == "Fast-er Learner" || player.event.name == "Lack of Productivity") {
				if(!player.hasDoneEvent)switchToProjectComplete();
				else switchToWorkerAssing(assignPrevKey);
			}
			else switchToWorkerAssing(assignPrevKey);
			break;
		case end:
			if(player.allWaiting)switchToEvent();
			else if(player.waiting)switchToWait();
			break;
		case win:
		case end:
			break;
	}

	console.log(player);
});

const emitTurn = (id, turn) =>{
	sock.emit('turn', [id, turn]);
}


const guestJoin = '../guestJoin.html';

const guestWait = '../guestWait.html';

const home = '../home.html';

const hostWait = '../hostWait.html';

const host = '../host.html';

const guest = '../guest.html';

//Gameplay Screens

const chooseComp = '../_chooseComp.html';

const confirmComp = '../_confirmComp.html';

const drawProjects = '../_drawProjects.html';

const drawProjects2 = '../_drawProjects2.html';

const drawProjectsLess = '../_drawProjects-1.html';

const drawProjectsLess2 = '../_drawProjects-2.html';

const drawWorkers = '../_drawWorkers.html';

const drawWorkers2 = '../_drawWorkers2.html';

const drawWorkersLess = '../_drawWorkers-1.html';

const drawWorkersLess2 = '../_drawWorkers-2.html';

const event = '../_event.html';

const firstDraw = '../_firstDraw.html';

const projectDetails = '../_projectDetails.html';

const trade = '../_trade.html';

const end = '../_endTurn.html';

const waitCompany = '../_waitComp.html';

const workerAssign = '../_workerAssign.html';

const workerDetails = '../_workerDetails.html';

const payConfirm = '../_payConfirm.html';

const wait = '../_wait.html';

const projectStatus = '../_projectStatus.html';

const workerStatus = '../_workerStatus.html';

const forcedFire = '../_forceFire.html';

const projectCompete = '../_projectComplete.html';

const modifyWorker = '../_modifyWorker.html';

const goalStatus = '../_goals.html';

const goals = '../_goals.html';

const win = '../_win.html';

const lose = '../_lose.html';

const checkEvent = '../_checkEvent.html';

const checkWorkers = '../_checkWorkers.html';

switchToHome();
