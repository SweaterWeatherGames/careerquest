const http = require('http');
const express = require('express');
const socketio = require('socket.io');

//const RpsGame = require('./rps-game');
const prototype = require('./prototype');

const app = express();

const clientPath = `${__dirname}/../client`;
console.log(`Serving static from ${clientPath}`);

app.use(express.static(clientPath));

const server = http.createServer(app);

const io = socketio(server);

let hosts = [];

io.on('connection', (sock) => {
	console.log('someone connected');

	/*if(hosts){
		//start a game
		new RpsGame(hosts, sock);

		hosts = null;

	} else {

		hosts = sock;

		hosts.emit('message', 'Waiting for a challenger...');
	}*/

	sock.on('req-host', () => {
		if(hosts.length < 9999){
			let setHost = false;
			while(setHost == false){
				const roomkey = Math.floor(Math.random() * 8999 + 1000);
				if(hosts[roomkey] == undefined){

					let game = new prototype(sock, roomkey);

					hosts[roomkey] = game;
					console.log(hosts.indexOf(game));
					setHost = true;

					sock.on('disconnect', (sock) => {
						console.log('a host disconnected');
						console.log('removed room ' + hosts.indexOf(game));
						game.killSession();

						hosts[hosts.indexOf(game)] = undefined;
					});

					sock.on('start-game', () => {
						game.startGame();
					});
				}
			}
		}
		
	});

	sock.on('req-join', (arr) => {
		let roomkey = arr[0];
		if(hosts[roomkey] != undefined){
			let connectionIndex = hosts[roomkey].addPlayer(sock, arr[1]);
			sock.on('disconnect', (sock) => {
				if(hosts[roomkey] != undefined && hosts[roomkey] != null)hosts[roomkey].removePlayer(connectionIndex);
			});

			sock.on('turn', (turn) => {
				if(hosts[roomkey] != undefined && hosts[roomkey] != null)hosts[roomkey].onTurn(connectionIndex, turn);
			});
		}else{
			sock.emit('join-fail', '');
		}
	})

	sock.on('message', (text) => {
		io.emit('message', text);
	});

	/*sock.on('disconnect', (sock) => {
		console.log('someone disconnected');
	});*/
});

server.on('error', (err) => {
	console.error('Server error: ', err);
});

server.listen(8080, () => {
	console.log('Prototype started on 8080');
});