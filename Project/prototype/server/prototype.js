

class prototype{
	constructor(host, roomkey){
		this._players = [];
		this._tradingPlayers = [];
		this._trade = [];

		this._auctioningPlayer;
		this._highestBid = 0;
		this._highestBidder = 0;
		this._auctionItems = [];

		this.host = host;
		this._roomkey = roomkey;
		this._nextPlayerID = 1;
		this._playerCount = 0;
		this._hasStarted = false;
		this.gameTier = 1;

		this.its_a_variety = false;
		this.limited_supply = false;
		this.desperate_interns = false;
		this.fast_learner = false;
		this.hr_demands_diversity = false;
		this.tax_returns = false;
		this.slow_season = false;
		this.calling_in_sick = false;
		this.fasterer_learner = false;
		this.downsizing = false;
		this.taxes_are_due = false;
		this.lack_of_productivity = false;
		this.raise_the_moral = false;
		this.bonus_check = false;

		this.host.emit('res-host', this._roomkey);
		this.tradeIndex = 0;
		this.someoneHasWon = false;
	}

	addPlayer(sock, username){
		if(this._hasStarted == true){
			sock.emit('already-playing', '');
			return 'not playing';
		}

		this._playerCount++;

		this._players.push(sock);

		var newPlayer = new Player(sock, this._players.indexOf(sock), username);
		sock.player = newPlayer;


		sock.emit('join-suc', /*this._players.indexOf(sock)*/this._roomkey);
		var arr = [];
		this._players.forEach((s)=>{
			if(this._checkNull(s) && this._checkNull(s.player))arr.push(s.player.name);
		})
		this.host.emit('play-count', arr);
		console.log('new player joined room ' +this. _roomkey + '. Total players == ' + this._playerCount);


		console.log(this._players.indexOf(sock));
		return this._players.indexOf(sock);
	}

	removePlayer(disconnectIndex){
		if(disconnectIndex === 'not playing'){
			return;
		}
		this._playerCount--;

		console.log("removed " + disconnectIndex);
		this._players.splice(disconnectIndex, 1, null);

		var arr = [];
		this._players.forEach((s)=>{
			if(this._checkNull(s) && this._checkNull(s.player))arr.push(s.player.name);
		})
		this.host.emit('play-count', arr);
		
		console.log('player left room '+this. _roomkey + '. Total players == ' + this._playerCount);

		if(this._hasStarted && this._checkRoundCompletion())this._startNextRound();
	}

	killSession(){
		this._players.forEach((s) => {
			if(this._checkNull(s))s.emit('game-end', '');
		});
	}

	onTurn(conIndex, incomingturn){
		///GAME LOGIC HERE!!!!!

		this._gameUpdate(conIndex, incomingturn);
	}

	//conIntex: the index of this socket connection in the _players array
	//turn: an array of turn information recieved from the user
	_gameUpdate(conIndex, turnPKG){
		let sock = this._players[conIndex];
		let key = turnPKG[0];
		let turn = turnPKG[1];


		if(!this._checkNull(sock))return;

		switch(key){
			case 'PickComp':
				sock.player.company.type = turn;
				sock.player.waiting = true;

				if(this._checkRoundCompletion())sock.player.allWaiting = true;
				else sock.emit('update', sock.player.buildUpdate());
				//else this._startNextRound();
				break;
			case 'AccDraw':
				var hireWorkers = function(worker, game){
					if(sock.player.company.money == -10 || game.hr_demands_diversity && worker.type === sock.player.company.type)return;
					sock.player.company.workers.push(worker);
					sock.player.company.money -= worker.hire;
					if(sock.player.company.money < -10)sock.player.company.money = -10;
				}
				var takeProject = function(pro){
					sock.player.company.projects.push(pro);
					pro.index = sock.player.projectIndex;
					sock.player.projectIndex++;
				}
				if(sock.player.company.workers.length < 5 && turn[0]) hireWorkers(sock.player.draw.workers[0], this);
				if(sock.player.company.workers.length < 5 && turn[1]) hireWorkers(sock.player.draw.workers[1], this);
				if(sock.player.company.workers.length < 5 && turn[2]) hireWorkers(sock.player.draw.workers[2], this);
				if(sock.player.company.workers.length < 5 && turn[3]) hireWorkers(sock.player.draw.workers[3], this);
				
				if(sock.player.company.projects.length < 5 && turn[4]) takeProject(sock.player.draw.projects[0]);
				if(sock.player.company.projects.length < 5 && turn[5]) takeProject(sock.player.draw.projects[1]);
				if(sock.player.company.projects.length < 5 && turn[6]) takeProject(sock.player.draw.projects[2]);
				if(sock.player.company.projects.length < 5 && turn[7]) takeProject(sock.player.draw.projects[3]);

				sock.emit('update', sock.player.buildUpdate());
				
				break;
			case 'Assign':
				var project = (turn[0] != -1) ? sock.player.company.projects[turn[0]] : null;
				var worker = sock.player.company.workers[turn[1]];

				//if(project.workers.indexOf(worker) === -1) project.workers.push(worker);
				if(project == null) worker.project = null;
				else if(worker.type == project.type && !worker.sick)worker.project = project;
				//console.log(turn);
				
			//console.log(project.workers);
			//console.log(worker.project);
				sock.emit('update', sock.player.buildUpdate());
				break;
			case 'Trade':
				break;
			case 'TradeRes':
				break;
			case 'TradeAcc':
				break;
			case 'TradeDec':
				break;
			case 'PayWork':
				//sock.player.waiting = true;
				
				if(sock.player.payWorkers()){
					sock.emit('update', sock.player.buildUpdate());
				}
				else {
					sock.player.mustFire = true;
					sock.emit('update', sock.player.buildUpdate());
				}
				break;
			case 'FireWork':
				sock.player.company.workers.splice(turn, 1);
				if(sock.player.mustFire){
					sock.player.mustFire = false;
					sock.player.waiting = true;
					if(this._checkRoundCompletion())sock.player.allWaiting = true;
				}
				else sock.emit('update', sock.player.buildUpdate());
				sock.player.hasDoneEvent = true;
				break;
			case 'endTurn':
				if(sock.player.company.workersPaid)sock.player.waiting = true;
				if(this._checkRoundCompletion())sock.player.allWaiting = true;
				else sock.emit('update', sock.player.buildUpdate());
				break;
			case 'modify':
				if(!this._checkNull(sock.player))return;
				var worker = sock.player.company.workers[turn];

				if(this.fast_learner) worker.skill++;
				if(this.fasterer_learner) worker.skill+=2;
				if(this.lack_of_productivity) worker.skill-=2;
				if(this.calling_in_sick){
					worker.sick = true;
					worker.project = null;
				}

				sock.emit('update', sock.player.buildUpdate());
				sock.player.hasDoneEvent = true;

				break;
			default:
				return;
		}


		if(this._checkRoundCompletion())this._startNextRound();
	}

	_checkRoundCompletion(){
		let allWaiting = true;
		let someoneWon = false;
		
		this._players.forEach((s, index) => {
			if(this._checkNull(s) && this._checkNull(s.player)){
				//console.log(s.player.name + ":" + s.player.waiting);
				if(!s.player.waiting) allWaiting = false;
				if(this._checkWon(s.player)) someoneWon = true;
			}
		});

		if(someoneWon){
			this._players.forEach((s, index) => {
				if(this._checkNull(s) && this._checkNull(s.player)){
					if(this._checkWon(s.player)) s.emit('win', '');
					else s.emit('lose', '');
				}
			});
		}

		return allWaiting;
	}

	_startNextRound(){

		this.host.emit('host-goal', 'Get as close as you can to TEST');

		this.its_a_variety = false;
		this.limited_supply = false;
		this.desperate_interns = false;
		this.fast_learner = false;
		this.hr_demands_diversity = false;
		this.tax_returns = false;
		this.slow_season = false;
		this.calling_in_sick = false;
		this.fasterer_learner = false;
		this.downsizing = false;
		this.taxes_are_due = false;
		this.lack_of_productivity = false;
		//this.raise_the_moral = false;        this change is perminant
		this.bonus_check = false;

		var event = this._generateEvent();
		if(this.gameTier === 1)event = new Event(null, ()=>{}, null);

		this._players.forEach((s) => {
			if(this._checkNull(s) && this._checkNull(s.player)){
				s.player.waiting = false;
				s.player.hasDoneEvent = false;

				s.player.event = event;
				s.player.event.function(s.player.company, this);

				s.player.company.doneProjects = [];

				s.player.pMoney = s.player.company.money;

				s.player.draw = this._generateDraw(s.player.company.type, Math.floor(this.gameTier));

				for(var i = s.player.company.projects.length - 1; i >= 0; i--){
					var p = s.player.company.projects[i];
					p.hasLeader = false;
					p.index = i;
					p.countDown--;
					if(p.countDown < 0)s.player.company.projects.splice(i, 1);
				}
				s.player.company.workers.forEach((w)=>{
					if(w.leader && w.project != null && !w.project.hasLeader) {
						w.project.hasLeader = true;
						w.project.duration++;
					}
				});

				s.player.company.workers.forEach((w)=>{
					w.paid = false;
					w.sick = false;

					var project = w.project;
					if(project != null){
						project.countDown = 3;
						project.duration -= w.skill;
						if(project.hasLeader)project.duration -= (this.raise_the_moral)? 2 : 1;
						if(project.hasLeader && s.player.company.type == "manu")project.duration--;
						if(project.duration <= 0){
							project.payoff(s.player.company);
							if(s.player.company.hasBonusWaiting){
								project.payoff(s.player.company);
								s.player.company.hasBonusWaiting = false;
							}
							if(project.tier == 3) s.player.level3s++;
							s.player.company.workers.forEach((w2)=>{
								if(w2.project == project)w2.project = null;
							});
							if (s.player.company.projects.indexOf(project) > -1) {
								s.player.company.doneProjects.push(project);
							    s.player.company.projects.splice(s.player.company.projects.indexOf(project), 1);
							}
						}
					}
					else {
						w.skill++;
						w.salary++;
						if(w.skill >= 6){
							w.skill = 6
							w.salary = 6;
							w.leader = true;
						}
					}
				});

				s.emit('update', s.player.buildUpdate());

				s.player.allWaiting = false;

				s.player.company.workersPaid = false;

				

			}
		});

		this.gameTier += .2;
		if(this.gameTier > 3)this.gameTier = 3;
		//console.log(this.gameTier);
	}

	startGame(){
		this._players.forEach((s) => {
			if(this._checkNull(s))s.emit('client-begin', this._players.indexOf(s));
		});
		this._hasStarted = true;
		//this._startNextRound();
	}

	_checkWon(player){
		var level3s = false;
		var cons = false;
		var manu = false;
		var heal = false;
		var it = false;
		var leader = false;
		var over10 = false;

		if(player.level3s >= 3)level3s = true;
		if(player.company.money > 25) over10 = true;
		player.company.workers.forEach((w)=>{
			if(w.type == 'i.t.') it = true;
			if(w.type == 'cons') cons = true;
			if(w.type == 'manu') manu = true;
			if(w.type == 'heal') heal = true;

			if(w.leader)leader = true;
		});

		player.hasLeader = leader;
		player.diversity = 0;
		if(it)player.diversity++;
		if(cons)player.diversity++;
		if(manu)player.diversity++;
		if(heal)player.diversity++;

		if(level3s && cons && manu && heal && it && leader && over10) return true;
		else return false;
	}

	_newGame(){

		this.host.emit('host-goal', 'Get as close as you can to DEBUG');

		this._players.forEach((s) => {

		});
	}

	_generateEvent(){
		var rarity = (Math.random() > .3) ? ((Math.random() > .3)? 'common' : 'uncommon') : 'rare';
		var tier = Math.floor(Math.random() * this.gameTier);
		var commonEvents = {
			tiers: [
			[{
				name: "It’s a Variety!",
				effect: "All players get an extra draw on both employees and projects.",
				func: (company, game) =>{
					game.its_a_variety = true;
				}
			}],
			[{
				name: "HR Demands Diversity",
				effect: "The next employee you hire, cannot be from the same company type you possess.",
				func: (company, game) =>{
					game.hr_demands_diversity = true;
				}
			}],
			[{
				name: "Lack of Productivity",
				effect: "One of your employee’s efficiency goes down -2. (An employee’s efficiency cannot fall below 1; player’s choice).",
				func: (company, game) =>{
					game.lack_of_productivity = true;
				}
			}]
			]
		};
		var uncommonEvents = {
			tiers: [
			[{
				name: "Limited Supply",
				effect: "All players are only dealt 2 employees and 2 projects this turn.",
				func: (company, game) =>{
					game.limited_supply = true;
				}
			},
			{
				name: "Desperate Interns",
				effect: "Initial cost of this round’s dealt employees are -1.",
				func: (company, game) =>{
					game.desperate_interns = true;
				}
			}],
			[{
				name: "Slow Season",
				effect: "All players are only dealt 1 employee and 1 project this turn.",
				func: (company, game) =>{
					game.slow_season = true;
				}
			},
			{
				name: "Calling in Sick",
				effect: "One of your employees loses their assigned project for a round, and cannot be reassigned a new on until the next round. (Player’s choice).",
				func: (company, game) =>{
					game.calling_in_sick = true;
				}
			}],
			[{
				name: "Bonus Check!",
				effect: "The next project you complete yields double the profits.",
				func: (company, game) =>{
					game.bonus_check = true;
					company.hasBonusWaiting = true;
				}
			}]
			]
		};
		var rareEvents = {
			tiers: [
			[{
				name: "Fast Learner",
				effect: "One of your employee’s efficiency goes up +1.",
				func: (company, game) =>{
					game.fast_learner = true;
				}
			}],
			[{
				name: "Tax Returns",
				effect: "Every player receives X money based on the Initial Pay of each of their employees.",
				func: (company, game) =>{
					game.tax_returns = true;
					company.workers.forEach((w)=>{
						company.money += w.hire;
					});
				}
			},
			{
				name: "Fast-er Learner",
				effect: "One of your employee’s efficiency goes up +2.",
				func: (company, game) =>{
					game.fasterer_learner = true;
				}
			}],
			[{
				name: "Downsizing",
				effect: "Every player must fire one employee.",
				func: (company, game) =>{
					game.downsizing = true;
				}
			},
			{
				name: "Taxes Are Due",
				effect: "Every player loses half of their money, rounded-down.",
				func: (company, game) =>{
					game.taxes_are_due = true;
					company.money -= Math.floor(company.money / 2);
				}
			},
			{
				name: "Raise the Morale!",
				effect: "Any Team Leader a player owns yields a +2 buff when paired with employees instead of +1. (This status lasts beyond this round, even if a player doesn’t own a Team Leader yet).",
				func: (company, game) =>{
					game.raise_the_moral = true;
				}
			}]
			]
		};
		var event = null;
		switch(rarity){
			case 'common': 
				var rand = Math.floor(Math.random() * commonEvents.tiers[tier].length);
				event = commonEvents.tiers[tier][rand];
				//console.log(event = commonEvents.tiers[tier]);
				break;
			case 'uncommon': 
				var rand = Math.floor(Math.random() * uncommonEvents.tiers[tier].length);
				event = uncommonEvents.tiers[tier][rand];
				//console.log(event = uncommonEvents.tiers[tier]);
				break;
			case 'rare':
				var rand = Math.floor(Math.random() * rareEvents.tiers[tier].length);
				event = rareEvents.tiers[tier][rand];
				//console.log(event = rareEvents.tiers[tier]);
				break;
		}
		var newEvent = new Event(event.name, event.func, event.effect);
		//var newEvent = new Event(rareEvents.tiers[2][0].name, rareEvents.tiers[2][0].func, rareEvents.tiers[2][0].effect);
		return newEvent;
	}

	_generateGlobalEvent(){

	}

	_generateWorker(baseType, randType, useBase, maxTier){
		var rand = Math.floor(Math.random() * maxTier * 2 + 1);
		var worker = new Worker();

		worker.type = (useBase)? baseType : randType;

		switch(rand){
			case 1:
				worker.hire = 1;
				worker.salary = 1;
				break;
			case 2:
				worker.hire = 4;
				worker.salary = 2;
				break;
			case 3:
				worker.hire = 6;
				worker.salary = 3;
				break;
			case 4:
				worker.hire = 8;
				worker.salary = 4;
				break;
			case 5:
				worker.hire = 10;
				worker.salary = 6;
				worker.leader = true;
				break;
			case 6:
				worker.hire = 10;
				worker.salary = 6;
				worker.skill = 5;
				worker.leader = true;
				break;
		}

		worker.skill = (rand);
		if(baseType != 'cons' && worker.type != 'cons' && rand != 1)worker.skill++;
		else if(baseType == 'cons' && worker.type == 'cons'){
			worker.hire -= 2;
			if(worker.hire < 0)worker.hire = 0;
		}
		if(this.desperate_interns){
			worker.hire--;
			if(worker.hire < 0)worker.hire = 0;
		}

		var img = '/../imgs/Card-';
		img += (Math.random() > .3)? ((Math.random() > .5)? 'Black_' : 'Tan_'): 'White_';
		img += worker.type.toUpperCase() + '.png';

		worker.imgURL = img;

		return worker;
	}

	_generateProjet(type, randType, useBase, maxTier){
		let tier = Math.floor(Math.random() * maxTier + 1)
		let rand = Math.random();
		var project = new Project();
		project.type = (useBase)? type : randType;
		project.tier = tier;

		switch(tier){
			case 1:{
				project.duration = (rand > .5)? 1 : 3;
				project.value = (rand > .5)? 2 : 5;
				break;
			}
			case 2:{
				project.duration = (rand > .5)? 4 : 8;
				project.value = (rand > .5)? 7 : 15;
				break;
			}
			case 3:{
				project.duration = (rand > .5)? 10 : 20;
				project.value = (rand > .5)? 18 : 30;
				if(rand < .12){
					project.duration = 30;
					project.value = 50;
				}
				break;
			}
		}

		project.payoff = (company) => {
			if(company.type == 'i.t.'){
				var num = 0;
				company.workers.forEach((w)=>{
					if(w.type == 'i.t.')num++;
				});
				company.workers.forEach((w)=>{
					if(w.type == 'i.t.' && num > 1)company.money++;
				});
			}
			company.money += project.value;
		};

		var img = '/../imgs/';
		switch(project.type){
			case 'cons':
				img += (Math.random() > .5)? ((Math.random() > .5)? 'Crane Ball' : 'Drilling') : ((Math.random() > .5)? 'Dump Truck' : 'Hammering');
				break;
			case 'heal':
				img += (Math.random() > .5)? ((Math.random() > .5)? 'Ambulance' : 'Medical Attention') : ((Math.random() > .5)? 'Pills' : 'Test Tubes');
				break;
			case 'i.t.':
				img += (Math.random() > .5)? ((Math.random() > .5)? 'Firewall' : 'IT Guy') : ((Math.random() > .5)? 'Tablet' : 'WiFi');
				break;
			case 'manu':
				img += (Math.random() > .5)? ((Math.random() > .5)? 'Arm' : 'Assembly Line') : ((Math.random() > .5)? 'Dolly' : 'Press');
				break;
		}
		
		img += '.png';

		project.imgURL = img;

		return project;
	}

	_generateDraw(type, level){
		let draw = {
			workers:[new Worker(), new Worker(), new Worker(), new Worker()], 
			projects:[new Project(), new Project(), new Project(), new Project]
		};

		for(var i = 0; i < 4; i++){
			let rand = Math.random();
			let randType = (rand > .5)? ((rand > .75)? 'heal' : 'manu') : ((rand > .25)? 'i.t.' : 'cons');

			draw.workers[i] = this._generateWorker(type, randType, (i == 0), level);
			draw.projects[i] = this._generateProjet(type, randType, (i != 0), level);			
		}

		return draw;
	}

	_checkNull(obj){
		const bool = (obj != null && obj != undefined);
		return bool;
	}
}

class Player{
	constructor(sock, index, name){
		this.sock = sock;
		this.name = name;
		this.number = index;
		this.event = new Event('', ()=>{});
		this.draw = {
			workers: [],
			projects: []
		};
		this.company = {
			type: "", //health, construction, manufaction, IT
			money: 10,
			v_pnt: 0,
			workers: [],
			projects: [],
			doneProjects: [],
			workersPaid: false,
			hasFired: false,
			hasBonusWaiting: false
		};
		this.competition = [];
		this.step = 0; //
		this.bonuses = [];
		this.firstRound = true;
		this.tradeQ = [];
		this.waiting = false;
		this.allWaiting = false;
		this.level = 1;
		this.mustFire = false;
		this.projectIndex = 0;
		this.hasDoneEvent = false;
		this.pMoney = 0;

		this.hasWon = false;
		this.level3s = 0;
		this.hasLeader = false;
		this.diversity = 0;

	}
	buildUpdate(){
		var update = {
			name:this.name,
			number:this.number,
			event:this.event,
			draw:this.draw,
			company:this.company,
			competition:this.competition,
			step:this.step,
			firstRound:this.firstRound,
			tradeQ:this.tradeQ,
			waiting:this.waiting,
			allWaiting:this.allWaiting,
			mustFire:this.mustFire,
			hasDoneEvent:this.hasDoneEvent,
			level3s:this.level3s,
			hasLeader:this.hasLeader,
			diversity:this.diversity,
			pMoney:this.pMoney
		}

		return update;
	}
	payWorkers(){
		if(this.company.money == -10)return false;

		this.company.workers.forEach((w)=>{
			this.company.money -= w.salary;
			w.paid = true;
		});
		if(this.company.money < -10)this.company.money = -10;
		this.company.workersPaid = true;
		return true;
	}
}

class CompetingPlayer{
	constructor(index, name){
		this.number = index;
		this.name = name;
		this.money = 0;
		this.workers = [];
	}
	update(money, workers){
		this.money = money;
		this.workers = workers;
	}
}

class Worker{
	constructor(){
		this.type = "";
		this.hire = 0;
		this.salary = 0;
		this.skill = 0;
		this.project = null;
		this.company = {};
		this.paid = false;
		this.imgURL = '';
		this.leader = false;
		this.image = '';
		this.sick = false;
	}
}

class Project{
	constructor(){
		this.type = "";
		this.tier = 0;
		this.payoff = null;
		this.duration = 0;
		this.workers = [];
		this.company = {};
		this.countDown = 3;
		this.imgURL = '';
		this.value = 0;
		this.image = '';
		this.index = 0;
		this.hasLeader = false;
	}
}

class Trade{
	constructor(p1, p2, worker){
		this.player1 = p1; 				//The person who initiated the trade
		this.player2 = 0; 				//The person recieving the trade offer
		this.worker1 = worker;				//The worker the initiator offered
		this.worker2 = null; 				//The reciever's counter offer
		this.qIndex1 = 0; 				//The index of this trade in p1's queue
		this.qIndex2 = 0; 				//The index of this trade in p2's queue
		this.tradeID = 0;
	}
	addP2(worker){
		this.worker2 = worker;
	}
}

class Event{
	constructor(name, funct, description){
		this.name = name;
		this.function = funct;
		this.description = description;
	}
	execute(player){
		this.function(player);
	}
}


module.exports = prototype;