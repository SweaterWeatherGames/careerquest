class RpsGame {
	constructor(p1, p2){
		this._players = [p1, p2];
		this._turns = [null, null];

		this._sendToPlayers('RIVAL would like to battle!');

		this._players.forEach((player, idx) => {
			player.on('turn', (turn) => {
				this._onTurn(idx, turn);
			});
		});
	}

	_sendToPlayer(playerIndex, msg){
		this._players[playerIndex].emit('message', msg);
	}

	_sendToPlayers(msg){
		this._players.forEach((s) => {
			s.emit('message', msg);
		});
	}

	_onTurn(playerIndex, turn){
		this._turns[playerIndex] = turn;
		this._sendToPlayer(playerIndex, `You selected ${turn}`);

		this._checkGameOver();
	}

	_checkGameOver(){
		const turns = this._turns;

		if(turns [0] && turns[1]){
			this._sendToPlayers('Match over! ' + turns.join(' : '));

			this._getGameResult();

			this._turns = [null, null];

			this._sendToPlayers('It\'s not over yet, GO!  Pokemon!');
		}
	}

	_getGameResult() {
		const p0 = this._decodeTurn(this._turns[0]);
		const p1 = this._decodeTurn(this._turns[1]);

		const distance = (p1 - p0 +3) % 3;

		switch (distance) {
			case 0:
				//draw
				this._sendToPlayers('Draw!  Try again!');
				break;
			case 1:
				//p0
				this._sendWinMessage(this._players[0],
					this._players[1]);
				break;
			case 2:
				//p1
				this._sendWinMessage(this._players[1],
					this._players[0]);
				break;
		}
	}

	_sendWinMessage(winner, loser){
		winner.emit('message', 'Super effective!  You win!');
		loser.emit('message', 'Not very effective... You lose...');
	}

	_decodeTurn(turn) {
		switch (turn) {
			case 'fire':
				return 2;
			case 'water':
				return 1;
			case 'grass':
				return 0;
			default:
				throw new Error(`Could not decode turn ${turn}`);
		}
	}
}

module.exports = RpsGame;